<?php

namespace Drupal\commerce_cart_estimate\Exception;

/**
 * Exception thrown when a save is attempted on a shipment estimate.
 */
class ShipmentSaveException extends \RuntimeException {}
