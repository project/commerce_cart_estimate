<?php

namespace Drupal\commerce_cart_estimate\Exception;

/**
 * Exception thrown when no applicable shipping rates could be calculated.
 */
class NoApplicableShippingRatesException extends CartEstimateException {}
