<?php

namespace Drupal\commerce_cart_estimate\Exception;

/**
 * Exception thrown when a save is attempted on an order estimate.
 */
class OrderSaveException extends \RuntimeException {}
