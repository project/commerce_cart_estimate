<?php

namespace Drupal\commerce_cart_estimate\Exception;

/**
 * Exception thrown when the shipping could not be estimated.
 */
class CartEstimateException extends \RuntimeException {}
