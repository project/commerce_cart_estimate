Commerce Cart Estimate
======================

Defines a form for estimating shipping rates/taxes on the shopping cart form.

## Setup

1. Install the module.
2. Edit your cart form view (admin/structure/views/view/commerce_cart_form)
3. Configure the Cart estimate views area handler.
